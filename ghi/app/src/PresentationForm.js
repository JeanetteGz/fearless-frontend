import React, { useEffect, useState } from 'react';

function Presentation(props) {
     const [conferences, setConferences] = useState([]);
     const [name, setName] = useState('');
     const [email, setEmail] = useState('');
     const [companyName, setCompanyName] = useState('');
     const [title, setTitle] = useState('');
     const [synopsis, setSynopsis] = useState=('');
     const [selectedConference, setSelectedConference] = useState('');

     const handleNameChange = (event) => {
          const value = event.target.value;
          setName(value);
     }

     const handleEmailChange = (event) => {
          const value = event.target.value;
          setEmail(value);
     }

     const handleCompanyNameChange = (event) => {
          const value = event.target.value;
          setCompanyName(value);
     }
     const handleTitleChange = (event) => {
          const value = event.target.value;
          setTitle(event);
     }
     const handleSynopsisChange = (event) => {
          const value = event.target.value;
          setSynopsis(event);
     }
     const handleSelectedConferenceChange = (event) => {
          const value = event.target.value;
          setSelectedConference(event);
     }
     const handleSubmit = (event) => {
          event.preventDefault();
          const data ={};
     }
}