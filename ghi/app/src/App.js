import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <div>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="attendees">
              <Route path="new" element={<AttendConferenceForm />} />
            </Route>
            <Route path="attendeesList">
              <Route element={<AttendeesList attendees={props.attendees} />} />
            </Route>
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
