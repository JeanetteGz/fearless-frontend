window.addEventListener('DOMContentLoaded', async () => {
     const url = 'http://localhost:8000/api/conferences/';
     let colSelect = 1;
     
     try {
     const response = await fetch(url);

     if (!response.ok) {
     throw new Error(`HTTP error! status: ${response.status}`);
     } else {
     const data = await response.json();
     for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details);
               console.log(details.conference.location);

          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const date = details.conference.date;
          const location = details.conference.location;
          const html = createCard(title, description, pictureUrl, date, location);

          const column = document.querySelector(`.col${colSelect}`);
          column.innerHTML += html;
          colSelect += 1;
          if (colSelect === 4) {
               colSelect = 1;
          }
          }
     }
     }
     } catch (e) {
       // Figure out what to do if an error is raised
     }
});

function createCard(name, description, pictureUrl, date, location) {
     const locationString = `${location.name}`;
     return `
     <div class="card">
     <img src="${pictureUrl}" class="card-img-top">
     <div class="card-body">
          h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationString}</h6>
          <p class="card-text">${description}</p>
     </div>
     <div class="card-footer">
          <p>${date}</p>
     </div>
     </div>
     `;
}
