// Get the cookie out of the cookie store
const payloadCookie = document.cookie.split('; ')
.find(cookie => cookie.startsWith('payload='));

if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
     const encodedPayload = JSON.parse(payloadCookie.split('=')[1]);

  // Convert the encoded payload from base64 to normal string
     const decodedPayload = atob(encodedPayload);

  // The payload is a JSON-formatted string, so parse it
     const payload = JSON.parse(decodedPayload);

  // Print the payload
     console.log(payload);

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
if (payload.permissions.includes('events.add_conference')) {
     const addConferenceLink = document.getElementById('add-conference-link');
     addConferenceLink.classList.remove('d-none');
}

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
if (payload.permissions.includes('events.add_location')) {
     const addLocationLink = document.getElementById('add-location-link');
     addLocationLink.classList.remove('d-none');
     }
}
